From 71b2e1fd89663237774b872613d8c03396e2a97c Mon Sep 17 00:00:00 2001
From: Suraj Jaiswal <jsuraj@codeaurora.org>
Date: Wed, 17 Mar 2021 15:48:15 +0530
Subject: [PATCH 23/29] net: stmmac: Enable PPS0 on bootup

Enable PPS0 for 19.2 MHz frequency on initializing PTP.
Remove PPS devices on driver remove.
Use default values when ptp rates are missing.

Change-Id: I15d5fc2d15dd38926970ac7a61f9362604a860a8
Signed-off-by: Suraj Jaiswal <jsuraj@codeaurora.org>
---
 drivers/net/ethernet/stmicro/stmmac/Makefile       |  3 +
 .../ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c    | 13 +++--
 .../ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h    |  4 ++
 .../net/ethernet/stmicro/stmmac/dwmac-qcom-pps.c   | 64 +++++++++++++++++++++-
 drivers/net/ethernet/stmicro/stmmac/stmmac_main.c  |  7 +++
 .../net/ethernet/stmicro/stmmac/stmmac_platform.c  | 10 ++++
 include/linux/stmmac.h                             |  2 +
 7 files changed, 97 insertions(+), 6 deletions(-)

diff --git a/drivers/net/ethernet/stmicro/stmmac/Makefile b/drivers/net/ethernet/stmicro/stmmac/Makefile
index 749d5a9..b8741ed 100644
--- a/drivers/net/ethernet/stmicro/stmmac/Makefile
+++ b/drivers/net/ethernet/stmicro/stmmac/Makefile
@@ -40,4 +40,7 @@ dwmac-altr-socfpga-objs := altr_tse_pcs.o dwmac-socfpga.o
 obj-$(CONFIG_STMMAC_PCI)	+= stmmac-pci.o
 obj-$(CONFIG_DWMAC_INTEL)	+= dwmac-intel.o
 obj-$(CONFIG_DWMAC_LOONGSON)	+= dwmac-loongson.o
+
+ccflags-$(CONFIG_PTP_1588_CLOCK)+=-DCONFIG_PTPSUPPORT_OBJ
+
 stmmac-pci-objs:= stmmac_pci.o
diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
index 2c14dea..830826b 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.c
@@ -77,6 +77,7 @@
 #define RGMII_CONFIG2_TX_CLK_PHASE_SHIFT_EN	BIT(5)

 #define EMAC_I0_EMAC_CORE_HW_VERSION_RGOFFADDR 0x00000070
+#define EMAC_HW_v2_3_2_RG 0x20030002
 #define EMAC_HW_v3_0_0_RG 0x30000000

 #define MII_BUSY 0x00000001
@@ -152,12 +153,13 @@ static int ethqos_handle_prv_ioctl(struct net_device *dev, struct ifreq *ifr, in
 	if (copy_from_user(&req, ifr->ifr_ifru.ifru_data,
 			   sizeof(struct ifr_data_struct)))
 		return -EFAULT;
-	if (copy_from_user(&eth_pps_cfg, (void __user *)req.ptr,
-			   sizeof(struct pps_cfg)))
-		return -EFAULT;

 	switch (req.cmd) {
 	case ETHQOS_CONFIG_PPSOUT_CMD:
+		if (copy_from_user(&eth_pps_cfg, (void __user *)req.ptr,
+				   sizeof(struct pps_cfg)))
+			return -EFAULT;
+
 		ret = ppsout_config(pdata, &eth_pps_cfg);
 		break;
 	}
@@ -1026,6 +1028,7 @@ static int qcom_ethqos_probe(struct platform_device *pdev)
 	plat_dat->pmt = 1;
 	plat_dat->tso_en = of_property_read_bool(np, "snps,tso");
 	plat_dat->handle_prv_ioctl = ethqos_handle_prv_ioctl;
+	plat_dat->init_pps = ethqos_init_pps;
 	plat_dat->phy_intr_enable = ethqos_phy_intr_enable;

 	/* Get rgmii interface speed for mac2c from device tree */
@@ -1088,7 +1091,7 @@ static int qcom_ethqos_probe(struct platform_device *pdev)
 	if (priv->plat->mac2mac_en)
 		priv->plat->mac2mac_link = 0;

-	if (ethqos->emac_ver == EMAC_HW_v3_0_0_RG) {
+	if (ethqos->emac_ver == EMAC_HW_v2_3_2_RG) {
 		ethqos_pps_irq_config(ethqos);
 		create_pps_interrupt_device_node(&ethqos->avb_class_a_dev_t,
 						 &ethqos->avb_class_a_cdev,
@@ -1140,6 +1143,8 @@ static int qcom_ethqos_remove(struct platform_device *pdev)
 		free_irq(ethqos->phy_intr, ethqos);
 	}

+	if (ethqos->emac_ver == EMAC_HW_v2_3_2_RG)
+		ethqos_remove_pps_dev(ethqos);
 	ethqos_cleanup_debugfs(ethqos);
 	ethqos_free_gpios(ethqos);
 	emac_emb_smmu_exit();
diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
index 0ae883c..cde5186 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-ethqos.h
@@ -45,6 +45,8 @@
 #define AVB_CLASS_A_CHANNEL_NUM 2
 #define AVB_CLASS_B_CHANNEL_NUM 3

+#define PPS_19_2_FREQ 19200000
+
 static inline u32 PPSCMDX(u32 x, u32 val)
 {
 	return (GENMASK(PPS_MINIDX(x) + 3, PPS_MINIDX(x)) &
@@ -148,6 +150,8 @@ int create_pps_interrupt_device_node(dev_t *pps_dev_t,
 				     struct cdev **pps_cdev,
 				     struct class **pps_class,
 				     char *pps_dev_node_name);
+void ethqos_remove_pps_dev(struct qcom_ethqos *ethqos);
 int ppsout_config(struct stmmac_priv *priv, struct pps_cfg *eth_pps_cfg);
+int ethqos_init_pps(void *priv_n);
 struct qcom_ethqos *get_pethqos(void);
 #endif
diff --git a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-pps.c b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-pps.c
index 58ea02a..54be888 100644
--- a/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-pps.c
+++ b/drivers/net/ethernet/stmicro/stmmac/dwmac-qcom-pps.c
@@ -86,6 +86,7 @@ static int ppsout_stop(struct stmmac_priv *priv, struct pps_cfg *eth_pps_cfg)
 	u32 val;
 	void __iomem *ioaddr = priv->ioaddr;

+	val = readl_relaxed(ioaddr + MAC_PPS_CONTROL);
 	val |= PPSCMDX(eth_pps_cfg->ppsout_ch, 0x5);
 	val |= TRGTMODSELX(eth_pps_cfg->ppsout_ch, 0x3);
 	val |= PPSEN0;
@@ -145,16 +146,29 @@ static void ethqos_register_pps_isr(struct stmmac_priv *priv, int ch)
 	}
 }

+static void ethqos_unregister_pps_isr(struct stmmac_priv *priv, int ch)
+{
+	struct qcom_ethqos *ethqos = priv->plat->bsp_priv;
+
+	if (ch == DWC_ETH_QOS_PPS_CH_2) {
+		free_irq(ethqos->pps_class_a_irq, priv);
+	} else if (ch == DWC_ETH_QOS_PPS_CH_3) {
+		free_irq(ethqos->pps_class_b_irq, priv);
+	}
+}
+
 int ppsout_config(struct stmmac_priv *priv, struct pps_cfg *eth_pps_cfg)
 {
 	int interval, width;
-	u32 sub_second_inc, value;
+	u32 sub_second_inc, value, val;
 	void __iomem *ioaddr = priv->ioaddr;
-	u32 val;
 	u64 temp;

 	if (!eth_pps_cfg->ppsout_start) {
 		ppsout_stop(priv, eth_pps_cfg);
+		if (eth_pps_cfg->ppsout_ch == DWC_ETH_QOS_PPS_CH_2 ||
+		    eth_pps_cfg->ppsout_ch == DWC_ETH_QOS_PPS_CH_3)
+			ethqos_unregister_pps_isr(priv, eth_pps_cfg->ppsout_ch);
 		return 0;
 	}

@@ -208,6 +222,34 @@ int ppsout_config(struct stmmac_priv *priv, struct pps_cfg *eth_pps_cfg)
 	return 0;
 }

+int ethqos_init_pps(void *priv_n)
+{
+	struct stmmac_priv *priv;
+	u32 value;
+	struct pps_cfg eth_pps_cfg = {0};
+
+	if (!priv_n)
+		return -ENODEV;
+
+	priv = priv_n;
+
+	priv->ptpaddr = priv->ioaddr + PTP_GMAC4_OFFSET;
+	value = (PTP_TCR_TSENA | PTP_TCR_TSCFUPDT | PTP_TCR_TSUPDT);
+	priv->hw->ptp->config_hw_tstamping(priv->ptpaddr, value);
+	priv->hw->ptp->init_systime(priv->ptpaddr, 0, 0);
+	priv->hw->ptp->adjust_systime(priv->ptpaddr, 0, 0, 0, 1);
+
+	/*Configuaring PPS0 PPS output frequency to default 19.2 Mhz*/
+	eth_pps_cfg.ppsout_ch = 0;
+	eth_pps_cfg.ptpclk_freq = priv->plat->clk_ptp_req_rate;
+	eth_pps_cfg.ppsout_freq = PPS_19_2_FREQ;
+	eth_pps_cfg.ppsout_start = 1;
+	eth_pps_cfg.ppsout_duty = 50;
+
+	ppsout_config(priv, &eth_pps_cfg);
+	return 0;
+}
+
 static ssize_t pps_fops_read(struct file *filp, char __user *buf,
 			     size_t count, loff_t *f_pos)
 {
@@ -384,3 +426,21 @@ int create_pps_interrupt_device_node(dev_t *pps_dev_t,
 alloc_chrdev1_region_fail:
 		return ret;
 }
+
+void ethqos_remove_pps_dev(struct qcom_ethqos *ethqos)
+{
+	if (!ethqos) {
+		ETHQOSERR("Null Param");
+		return;
+	}
+
+	device_destroy(ethqos->avb_class_a_class, ethqos->avb_class_a_dev_t);
+	class_destroy(ethqos->avb_class_a_class);
+	cdev_del(ethqos->avb_class_a_cdev);
+	unregister_chrdev_region(ethqos->avb_class_a_dev_t, 1);
+
+	device_destroy(ethqos->avb_class_b_class, ethqos->avb_class_b_dev_t);
+	class_destroy(ethqos->avb_class_b_class);
+	cdev_del(ethqos->avb_class_b_cdev);
+	unregister_chrdev_region(ethqos->avb_class_b_dev_t, 1);
+}
diff --git a/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c b/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
index 11566a9..6640957 100644
--- a/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
+++ b/drivers/net/ethernet/stmicro/stmmac/stmmac_main.c
@@ -3344,6 +3344,8 @@ static int stmmac_hw_setup(struct net_device *dev, bool init_ptp)
 			netdev_warn(priv->dev, "PTP init failed\n");
 		else
 			ret = clk_set_rate(priv->plat->clk_ptp_ref, 96000000);
+
+		ret = priv->plat->init_pps(priv);
 	}

 	priv->eee_tw_timer = STMMAC_DEFAULT_TWT_LS;
@@ -3792,7 +3794,12 @@ int stmmac_open(struct net_device *dev)
 		goto init_error;
 	}

+#ifdef CONFIG_PTPSUPPORT_OBJ
 	ret = stmmac_hw_setup(dev, true);
+#else
+	ret = stmmac_hw_setup(dev, false);
+#endif
+
 	if (ret < 0) {
 		netdev_err(priv->dev, "%s: Hw setup failed\n", __func__);
 		goto init_error;
diff --git a/drivers/net/ethernet/stmicro/stmmac/stmmac_platform.c b/drivers/net/ethernet/stmicro/stmmac/stmmac_platform.c
index 1e33e56..136c6a1 100644
--- a/drivers/net/ethernet/stmicro/stmmac/stmmac_platform.c
+++ b/drivers/net/ethernet/stmicro/stmmac/stmmac_platform.c
@@ -603,6 +603,16 @@ stmmac_probe_config_dt(struct platform_device *pdev, u8 *mac)
 		dev_dbg(&pdev->dev, "PTP rate %d\n", plat->clk_ptp_rate);
 	}

+	if (of_property_read_u32(np,
+				 "snps,ptp-ref-clk-rate",
+				 &plat->clk_ptp_rate))
+		plat->clk_ptp_rate = 250000000;
+
+	if (of_property_read_u32(np,
+				 "snps,ptp-req-clk-rate",
+				 &plat->clk_ptp_req_rate))
+		plat->clk_ptp_req_rate = 96000000;
+
 	plat->stmmac_rst = devm_reset_control_get_optional(&pdev->dev,
 							   STMMAC_RESOURCE_NAME);
 	if (IS_ERR(plat->stmmac_rst)) {
diff --git a/include/linux/stmmac.h b/include/linux/stmmac.h
index 6e1bed5..968bfd7 100644
--- a/include/linux/stmmac.h
+++ b/include/linux/stmmac.h
@@ -250,6 +250,7 @@ struct plat_stmmacenet_data {
 	struct clk *pclk;
 	struct clk *clk_ptp_ref;
 	unsigned int clk_ptp_rate;
+	unsigned int clk_ptp_req_rate;
 	unsigned int clk_ref_rate;
 	unsigned int mult_fact_100ns;
 	s32 ptp_max_adj;
@@ -289,6 +290,7 @@ struct plat_stmmacenet_data {
 	struct emac_emb_smmu_cb_ctx stmmac_emb_smmu_ctx;
 	bool phy_intr_en_extn_stm;
 	bool phy_intr_en;
+	int (*init_pps)(void *priv);
 	int (*handle_prv_ioctl)(struct net_device *dev, struct ifreq *ifr,
 				int cmd);
 	int (*phy_intr_enable)(void *priv);
--
2.7.4

