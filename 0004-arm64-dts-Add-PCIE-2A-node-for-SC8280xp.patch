From 5134df6481d6d95cceb4973af104b9e4e2f79535 Mon Sep 17 00:00:00 2001
From: Shazad Hussain <quic_shazhuss@quicinc.com>
Date: Fri, 16 Sep 2022 14:58:43 +0530
Subject: [PATCH 4/4] arm64: dts: Add PCIE 2A node for SC8280xp

added changes to enable pcie 2a for ACP3 board, disabled
3a instance. Correct USB0 ssphy reset to avoid target crash.
Github: https://github.com/andersson/kernel/commits/wip/sc8280xp-next-20220420

Change-Id: I7366efdb06cd8fff487579fa2f7ac500f98ef9bc
Signed-off-by: Shazad Hussain <quic_shazhuss@quicinc.com>
---
 arch/arm64/boot/dts/qcom/sa8295p-adp.dts |  15 ++-
 arch/arm64/boot/dts/qcom/sa8540p.dtsi    |   2 +-
 arch/arm64/boot/dts/qcom/sc8280xp.dtsi   | 152 ++++++++++++++++++++++++++++++-
 3 files changed, 162 insertions(+), 7 deletions(-)

diff --git a/arch/arm64/boot/dts/qcom/sa8295p-adp.dts b/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
index acae390..1b0b588 100644
--- a/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
+++ b/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
@@ -187,19 +187,26 @@
 	status = "okay";
 };
 
-&pcie3a {
-	wake-gpios = <&tlmm 56 GPIO_ACTIVE_HIGH>;
-
+&pcie2a {
 	status = "okay";
 };
 
-&pcie3a_phy {
+&pcie2a_phy {
 	vdda-phy-supply = <&vreg_l11a>;
 	vdda-pll-supply = <&vreg_l3a>;
 
 	status = "okay";
 };
 
+&pcie3a {
+	wake-gpios = <&tlmm 56 GPIO_ACTIVE_HIGH>;
+};
+
+&pcie3a_phy {
+	vdda-phy-supply = <&vreg_l11a>;
+	vdda-pll-supply = <&vreg_l3a>;
+};
+
 &qup2 {
 	status = "okay";
 };
diff --git a/arch/arm64/boot/dts/qcom/sa8540p.dtsi b/arch/arm64/boot/dts/qcom/sa8540p.dtsi
index bfd0a22..093aa96 100644
--- a/arch/arm64/boot/dts/qcom/sa8540p.dtsi
+++ b/arch/arm64/boot/dts/qcom/sa8540p.dtsi
@@ -136,5 +136,5 @@
 	      <0 0x40100000 0 0x100000>;
 
 	ranges = <0x01000000 0x0 0x40200000 0 0x40200000 0x0 0x100000>,
-		<0x02000000 0x6 0x00000000 0x6 0x00000000 0x2 0x00000000>;
+		<0x03000000 0x6 0x00000000 0x6 0x00000000 0x2 0x00000000>;
 };
diff --git a/arch/arm64/boot/dts/qcom/sc8280xp.dtsi b/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
index 5fb1e21..8876c0d 100644
--- a/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
+++ b/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
@@ -705,12 +705,14 @@
 			#power-domain-cells = <1>;
 			clocks = <&rpmhcc RPMH_CXO_CLK>,
 				 <&sleep_clk>,
+				 <&pcie2a_lane>,
 				 <&pcie3a_lane>,
 				 <&usb_0_ssphy>,
 				 <&usb_1_ssphy>;
 			clock-names = "bi_tcxo",
 				      "sleep_clk",
-				      "pcie_3a_pipe_clk",
+					  "pcie_2a_pipe_clk",
+					  "pcie_3a_pipe_clk",
 				      "usb3_phy_wrapper_gcc_usb30_pipe_clk",
 				      "usb3_uni_phy_sec_gcc_usb30_pipe_clk";
 			power-domains = <&rpmhpd SC8280XP_CX>;
@@ -1316,6 +1318,128 @@
 			#freq-domain-cells = <1>;
 		};
 
+/* FIXME: sort by unit address */
+		pcie2a: pci@1c20000 {
+			compatible = "qcom,pcie-sc8280xp", "snps,dw-pcie";
+			reg = <0 0x01c20000 0 0x3000>,
+			      <0 0x3c000000 0 0xf1d>,
+			      <0 0x3c000f20 0 0xa8>,
+			      <0 0x3c001000 0 0x1000>,
+			      <0 0x3c100000 0 0x100000>;
+			reg-names = "parf", "dbi", "elbi", "atu", "config";
+			device_type = "pci";
+			linux,pci-domain = <2>;
+			bus-range = <0x00 0xff>;
+			num-lanes = <2>;	// FIXME: 4?
+
+			#address-cells = <3>;
+			#size-cells = <2>;
+
+			ranges = <0x01000000 0x0 0x3c200000 0 0x3c200000 0x0 0x100000>,
+				 <0x02000000 0x0 0x3c300000 0 0x3c300000 0x0 0x1d00000>;
+
+			interrupts = <GIC_SPI 440 IRQ_TYPE_LEVEL_HIGH>;
+			interrupt-names = "msi";
+			#interrupt-cells = <1>;
+			interrupt-map-mask = <0 0 0 0x7>;
+			interrupt-map = <0 0 0 1 &intc 0 0 GIC_SPI 530 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 2 &intc 0 0 GIC_SPI 531 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 3 &intc 0 0 GIC_SPI 532 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 4 &intc 0 0 GIC_SPI 533 IRQ_TYPE_LEVEL_HIGH>;
+
+			/* FIXME: add support to driver? */
+			interconnects = <&aggre2_noc MASTER_PCIE_2A 0 &mc_virt SLAVE_EBI1 0>;
+			interconnect-names = "pcie-ddr";	// FIXME: pcie-mem?
+
+			/*
+			 * FIXME: move pipe clocks to phy (first five)
+			 * FIXME: cnoc_4_qx, pci4 only
+			 */
+			clocks = <&gcc GCC_PCIE_2A_PIPE_CLK>,
+				 <&gcc GCC_PCIE_2A_PIPEDIV2_CLK>,
+				 <&gcc GCC_PCIE_2A_PIPE_CLK_SRC>,
+				 <&pcie2a_lane>,
+				 <&rpmhcc RPMH_CXO_CLK>,
+				 <&gcc GCC_PCIE_2A_AUX_CLK>,
+				 <&gcc GCC_PCIE_2A_CFG_AHB_CLK>,
+				 <&gcc GCC_PCIE_2A_MSTR_AXI_CLK>,
+				 <&gcc GCC_PCIE_2A_SLV_AXI_CLK>,
+				 <&gcc GCC_PCIE_2A_SLV_Q2A_AXI_CLK>,
+				 <&gcc GCC_DDRSS_PCIE_SF_TBU_CLK>,
+				 <&gcc GCC_AGGRE_NOC_PCIE_4_AXI_CLK>,
+				 <&gcc GCC_AGGRE_NOC_PCIE_SOUTH_SF_AXI_CLK>;
+			clock-names = "pipe",
+				      "pipediv2",
+				      "pipe_mux",
+				      "phy_pipe",
+				      "ref",
+				      "aux",
+				      "cfg",
+				      "bus_master",
+				      "bus_slave",
+				      "slave_q2a",
+				      "ddrss_sf_tbu",
+				      "aggre0",
+				      "aggre1";
+
+			assigned-clocks = <&gcc GCC_PCIE_2A_AUX_CLK>;
+			assigned-clock-rates = <19200000>;
+
+			resets = <&gcc GCC_PCIE_2A_BCR>;
+			reset-names = "pci";
+
+			power-domains = <&gcc PCIE_2A_GDSC>;
+
+			phys = <&pcie2a_lane>;
+			phy-names = "pciephy";
+
+			perst-gpios = <&tlmm 143 GPIO_ACTIVE_LOW>;
+			wake-gpios = <&tlmm 145 GPIO_ACTIVE_HIGH>;
+
+			pinctrl-names = "default";
+			pinctrl-0 = <&pcie2a_default_state>;
+
+		};
+
+		pcie2a_phy: phy@1c24000 {
+			compatible = "qcom,sc8280xp-qmp-modem-pcie-phy";
+			reg = <0 0x01c24000 0 0x1c0>;	// FIXME: 0x1c8?
+			#address-cells = <2>;
+			#size-cells = <2>;
+			ranges;
+			clocks = <&gcc GCC_PCIE_2A_AUX_CLK>,
+				 <&gcc GCC_PCIE_2A_CFG_AHB_CLK>,
+				 <&gcc GCC_PCIE_2A2B_CLKREF_CLK>,
+				 <&gcc GCC_PCIE2A_PHY_RCHNG_CLK>;
+			clock-names = "aux", "cfg_ahb", "ref", "refgen";
+
+			resets = <&gcc GCC_PCIE_2A_PHY_NOCSR_COM_PHY_BCR>;
+			reset-names = "phy";
+
+			/* FIXME: max by default? */
+			assigned-clocks = <&gcc GCC_PCIE2A_PHY_RCHNG_CLK>;
+			assigned-clock-rates = <100000000>;
+
+			pcie2a_lane: phy@1c24e00 {
+				/* FIXME: verify, cf. qcom,usb3-5nm-qmp-uni.h */
+				reg = <0 0x1c24e00 0 0x170>, /* tx0 */	// FIXME: 0x160?
+				      <0 0x1c25000 0 0x200>, /* rx0 */	// FIXME: 0x1ec?
+				      <0 0x1c24200 0 0x1f0>, /* pcs */
+				      <0 0x1c25600 0 0x170>, /* tx1 */
+				      <0 0x1c25800 0 0x200>, /* rx1 */
+				      <0 0x1c24600 0 0xf8>; /* FIXME: pcs_misc? size? */
+
+				clocks = <&gcc GCC_PCIE_2A_PIPE_CLK>,
+					 <&gcc GCC_PCIE_2A_PIPEDIV2_CLK>;
+				clock-names = "pipe0", "pipediv2_0";
+
+				#phy-cells = <0>;
+
+				#clock-cells = <0>;
+				clock-output-names = "pcie_2a_pipe_clk";
+			};
+		};
+
 		pcie3a: pci@1c10000 {
 			compatible = "qcom,pcie-sc8280xp", "snps,dw-pcie";
 			reg = <0 0x01c10000 0 0x3000>,
@@ -2012,7 +2136,7 @@
 			clock-names = "aux", "ref_clk_src", "ref", "com_aux";
 
 			resets = <&gcc GCC_USB3_PHY_PRIM_BCR>,
-			         <&gcc GCC_USB3_DP_PHY_PRIM_BCR>;
+			         <&gcc GCC_USB4_DP_PHY_PRIM_BCR>;
 			reset-names = "phy", "common";
 
 			power-domains = <&gcc USB30_PRIM_GDSC>;
@@ -2587,6 +2711,30 @@
 
 /* FIXME: include in node directly? */
 &tlmm {
+	/* FIXME: clkreq sleep state? */
+	pcie2a_default_state: pcie2a-default {
+		perst {
+			pins = "gpio143";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+
+		clkreq {
+			pins = "gpio142";
+			function = "pcie2a_clkreq";
+			drive-strength = <2>;
+			bias-pull-up;
+		};
+
+		wake {
+			pins = "gpio145";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-up;
+		};
+	};
+
 	pcie3a_default_state: pcie3a-default {
 		perst {
 			pins = "gpio151";
-- 
2.7.4

