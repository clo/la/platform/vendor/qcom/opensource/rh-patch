From 6aab2ae4c0acfac66c0311edbd326fdf5aed6c17 Mon Sep 17 00:00:00 2001
From: Shazad Hussain <quic_shazhuss@quicinc.com>
Date: Sat, 3 Sep 2022 11:12:32 +0530
Subject: [PATCH 3/4] hack: arm64: dts: Add PCIE 3A node for SC8280xp

added changes to enable pcie 3a.
Github: https://github.com/andersson/kernel/commits/wip/sc8280xp-next-20220420

Change-Id: I53c1f8f46297af6376f06a8edca9495f2a702050
Signed-off-by: Shazad Hussain <quic_shazhuss@quicinc.com>
---
 arch/arm64/boot/dts/qcom/sa8295p-adp.dts |  21 +++++
 arch/arm64/boot/dts/qcom/sa8540p.dtsi    |  11 +++
 arch/arm64/boot/dts/qcom/sc8280xp.dtsi   | 148 +++++++++++++++++++++++++++++++
 3 files changed, 180 insertions(+)

diff --git a/arch/arm64/boot/dts/qcom/sa8295p-adp.dts b/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
index a9c93fe..acae390 100644
--- a/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
+++ b/arch/arm64/boot/dts/qcom/sa8295p-adp.dts
@@ -70,6 +70,14 @@
 			regulator-initial-mode = <RPMH_REGULATOR_MODE_HPM>;
 			regulator-allow-set-load;
 		};
+
+		vreg_l11a: ldo11 {
+			regulator-name = "vreg_l11a";
+			regulator-min-microvolt = <880000>;
+			regulator-max-microvolt = <880000>;
+			regulator-initial-mode = <RPMH_REGULATOR_MODE_HPM>;
+			regulator-allow-set-load;
+		};
 	};
 
 	pmm8540-c-regulators {
@@ -179,6 +187,19 @@
 	status = "okay";
 };
 
+&pcie3a {
+	wake-gpios = <&tlmm 56 GPIO_ACTIVE_HIGH>;
+
+	status = "okay";
+};
+
+&pcie3a_phy {
+	vdda-phy-supply = <&vreg_l11a>;
+	vdda-pll-supply = <&vreg_l3a>;
+
+	status = "okay";
+};
+
 &qup2 {
 	status = "okay";
 };
diff --git a/arch/arm64/boot/dts/qcom/sa8540p.dtsi b/arch/arm64/boot/dts/qcom/sa8540p.dtsi
index ed8479d..bfd0a22 100644
--- a/arch/arm64/boot/dts/qcom/sa8540p.dtsi
+++ b/arch/arm64/boot/dts/qcom/sa8540p.dtsi
@@ -127,3 +127,14 @@
 		};
 	};
 };
+
+&pcie3a {
+	reg = <0 0x01c10000 0 0x3000>,
+	      <0 0x40000000 0 0xf1d>,
+	      <0 0x40000f20 0 0xa8>,
+	      <0 0x40001000 0 0x1000>,
+	      <0 0x40100000 0 0x100000>;
+
+	ranges = <0x01000000 0x0 0x40200000 0 0x40200000 0x0 0x100000>,
+		<0x02000000 0x6 0x00000000 0x6 0x00000000 0x2 0x00000000>;
+};
diff --git a/arch/arm64/boot/dts/qcom/sc8280xp.dtsi b/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
index e34b93f..5fb1e21 100644
--- a/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
+++ b/arch/arm64/boot/dts/qcom/sc8280xp.dtsi
@@ -705,10 +705,12 @@
 			#power-domain-cells = <1>;
 			clocks = <&rpmhcc RPMH_CXO_CLK>,
 				 <&sleep_clk>,
+				 <&pcie3a_lane>,
 				 <&usb_0_ssphy>,
 				 <&usb_1_ssphy>;
 			clock-names = "bi_tcxo",
 				      "sleep_clk",
+				      "pcie_3a_pipe_clk",
 				      "usb3_phy_wrapper_gcc_usb30_pipe_clk",
 				      "usb3_uni_phy_sec_gcc_usb30_pipe_clk";
 			power-domains = <&rpmhpd SC8280XP_CX>;
@@ -1314,6 +1316,126 @@
 			#freq-domain-cells = <1>;
 		};
 
+		pcie3a: pci@1c10000 {
+			compatible = "qcom,pcie-sc8280xp", "snps,dw-pcie";
+			reg = <0 0x01c10000 0 0x3000>,
+				  <0 0x34000000 0 0xf1d>,
+			      <0 0x34000f20 0 0xa8>,
+			      <0 0x34001000 0 0x1000>,
+			      <0 0x34100000 0 0x100000>;
+			reg-names = "parf", "dbi", "elbi", "atu", "config";
+			device_type = "pci";
+			linux,pci-domain = <4>;
+			bus-range = <0x00 0xff>;
+			num-lanes = <2>;
+
+			#address-cells = <3>;
+			#size-cells = <2>;
+
+			ranges = <0x01000000 0x0 0x34200000 0 0x34200000 0x0 0x100000>,
+			         <0x02000000 0x0 0x34300000 0 0x34300000 0x0 0x1d00000>;
+
+			interrupts = <GIC_SPI 567 IRQ_TYPE_LEVEL_HIGH>;
+			interrupt-names = "msi";
+			#interrupt-cells = <1>;
+			interrupt-map-mask = <0 0 0 0x7>;
+			interrupt-map = <0 0 0 1 &intc 0 0 GIC_SPI 541 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 2 &intc 0 0 GIC_SPI 542 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 3 &intc 0 0 GIC_SPI 543 IRQ_TYPE_LEVEL_HIGH>,
+					<0 0 0 4 &intc 0 0 GIC_SPI 544 IRQ_TYPE_LEVEL_HIGH>;
+
+			/* FIXME: add support to driver? */
+			interconnects = <&aggre2_noc MASTER_PCIE_3A 0 &mc_virt SLAVE_EBI1 0>;
+			interconnect-names = "pcie-ddr";	// FIXME: pci-mem?
+
+			clocks = <&gcc GCC_PCIE_3A_PIPE_CLK>,
+				 <&gcc GCC_PCIE_3A_PIPEDIV2_CLK>,
+				 <&gcc GCC_PCIE_3A_PIPE_CLK_SRC>,
+				 <&pcie3a_lane>,
+				 <&rpmhcc RPMH_CXO_CLK>,
+				 <&gcc GCC_PCIE_3A_AUX_CLK>,
+				 <&gcc GCC_PCIE_3A_CFG_AHB_CLK>,
+				 <&gcc GCC_PCIE_3A_MSTR_AXI_CLK>,
+				 <&gcc GCC_PCIE_3A_SLV_AXI_CLK>,
+				 <&gcc GCC_PCIE_3A_SLV_Q2A_AXI_CLK>,
+				 <&gcc GCC_DDRSS_PCIE_SF_TBU_CLK>,
+				 <&gcc GCC_AGGRE_NOC_PCIE_4_AXI_CLK>,
+				 <&gcc GCC_AGGRE_NOC_PCIE_SOUTH_SF_AXI_CLK>;
+			clock-names = "pipe",
+				      "pipediv2",
+				      "pipe_mux",
+				      "phy_pipe",
+				      "ref",
+				      "aux",
+				      "cfg",
+				      "bus_master",
+				      "bus_slave",
+				      "slave_q2a",
+				      "ddrss_sf_tbu",
+				      "aggre0",
+				      "aggre1";
+
+			assigned-clocks = <&gcc GCC_PCIE_3A_AUX_CLK>;
+			assigned-clock-rates = <19200000>;
+
+			resets = <&gcc GCC_PCIE_3A_BCR>;
+			reset-names = "pci";
+
+			power-domains = <&gcc PCIE_3A_GDSC>;
+
+			phys = <&pcie3a_lane>;
+			phy-names = "pciephy";
+
+			perst-gpios = <&tlmm 151 GPIO_ACTIVE_LOW>;
+			wake-gpios = <&tlmm 148 GPIO_ACTIVE_HIGH>;	// FIXME: adp 56
+
+			pinctrl-names = "default";
+			pinctrl-0 = <&pcie3a_default_state>;
+
+			status = "disabled";
+		};
+
+		pcie3a_phy: phy@1c14000 {
+			compatible = "qcom,sc8280xp-qmp-modem-pcie-phy";	// FIXME: modem
+			reg = <0 0x01c14000 0 0x1c0>;
+			#address-cells = <2>;
+			#size-cells = <2>;
+			ranges;
+
+			clocks = <&gcc GCC_PCIE_3A_AUX_CLK>,
+				 <&gcc GCC_PCIE_3A_CFG_AHB_CLK>,
+				 <&gcc GCC_PCIE_3A3B_CLKREF_CLK>,
+				 <&gcc GCC_PCIE3A_PHY_RCHNG_CLK>;
+			clock-names = "aux", "cfg_ahb", "ref", "refgen";
+
+			resets = <&gcc GCC_PCIE_3A_PHY_NOCSR_COM_PHY_BCR>;
+			reset-names = "phy";
+
+			assigned-clocks = <&gcc GCC_PCIE3A_PHY_RCHNG_CLK>;
+			assigned-clock-rates = <100000000>;
+
+			status = "disabled";
+
+			pcie3a_lane: phy@1c14e00 {
+				/* FIXME: verify */
+				reg = <0 0x1c14e00 0 0x170>, /* tx0 */
+				      <0 0x1c15000 0 0x200>, /* rx0 */
+				      <0 0x1c14200 0 0x1f0>, /* pcs */
+				      <0 0x1c15600 0 0x170>, /* tx1 */
+				      <0 0x1c15800 0 0x200>, /* rx1 */
+				      <0 0x1c14600 0 0x200>; /* FIXME: pcs_misc? size */
+
+				clocks = <&gcc GCC_PCIE_3A_PIPE_CLK>,
+					 <&gcc GCC_PCIE_3A_PIPEDIV2_CLK>;
+				clock-names = "pipe0", "pipediv2_0";
+
+				#phy-cells = <0>;
+
+				#clock-cells = <0>;
+				clock-output-names = "pcie_3a_pipe_clk";
+			};
+		};
+
 		tlmm: pinctrl@f100000 {
 			compatible = "qcom,sc8280xp-tlmm";
 			reg = <0 0x0f100000 0 0x300000>;
@@ -2462,3 +2584,29 @@
 		clock-frequency = <500000>;
 	};
 };
+
+/* FIXME: include in node directly? */
+&tlmm {
+	pcie3a_default_state: pcie3a-default {
+		perst {
+			pins = "gpio151";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+
+		clkreq {
+			pins = "gpio150";
+			function = "pcie3a_clkreq";
+			drive-strength = <2>;
+			bias-pull-up;
+		};
+
+		wake {
+			pins = "gpio148";	// FIXME: adp 56
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-up;
+		};
+	};
+};
-- 
2.7.4

